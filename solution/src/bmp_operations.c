#include "bmp/bmp_operations.h"
#include "bmp/bmp_header.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define bType 0x4D42
#define bPlanes 1
#define bSize 40
#define bBitCount sizeof(struct pixel) * 8;
#define bCompression 0
#define bXPelsPerMeter 0
#define bYPelsPerMeter 0
#define bClrUsed 0
#define bClrImportant 0
#define bReserved 0

#define PRI_SPECIFIER(e)                                                       \
  (_Generic((e),                                                               \
      uint16_t: "%" PRIu16,                                                    \
      uint32_t: "%" PRIu32,                                                    \
      default: "NOT IMPLEMENTED"))

#define GET_FIELD(name) header->name

#define PRINT_FIELD(t, name)                                                   \
  fprintf(f, "%-17s: ", #name);                                                \
  fprintf(f, PRI_SPECIFIER(GET_FIELD(name)), GET_FIELD(name));                 \
  fprintf(f, "\n");

void bmp_header_print(struct bmp_header const *header, FILE *f) {
  FOR_BMP_HEADER(PRINT_FIELD)
}

bool read_header(FILE *f, struct bmp_header *header) {
  return fread(header, sizeof(struct bmp_header), 1, f);
}

bool read_header_from_file(FILE *f, struct bmp_header *header) {
  return read_header(f, header);
}

enum open_status fopen_bmp_out(FILE **in, char const *file_name) {
  if (in == NULL) {
    return OPEN_ERROR;
  }

  *in = fopen(file_name, "wb");

  if (*in == NULL) {
    return OPEN_ERROR;
  }
  return OPEN_OK;
}

enum open_status fopen_bmp_in(FILE **in, char const *file_name) {
  if (in == NULL) {
    return OPEN_ERROR;
  }

  *in = fopen(file_name, "rb");
  // fopen_s(in, file_name, "rb");
  if (*in == NULL) {
    return OPEN_ERROR;
  }
  return OPEN_OK;
}

unsigned int get_padding(int32_t width) {
  return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE *file, struct image *img) {

  struct bmp_header bmp_header = {0};
  struct bmp_header *header = &bmp_header;
  struct pixel *pixels;

  read_header_from_file(file, header);

  bmp_header_print(header, stdout);
  fseek(file, header->bOffBits, SEEK_SET);
  if (header->biWidth == 0 || header->biHeight == 0) {
    return READ_INVALID_BITS;
  }

  pixels = (struct pixel *)malloc(header->biWidth * header->biHeight *
                                  sizeof(struct pixel)); // free at main

  unsigned int padding = get_padding((int32_t)header->biWidth);
  for (uint32_t i = 0; i < header->biHeight; i++) {
    if (fread(pixels + i * header->biWidth, sizeof(struct pixel),
              header->biWidth, file) != header->biWidth) {
      return READ_INVALID_BITS;
    }
    if (fseek(file, padding, SEEK_CUR) != 0) {
      return READ_INVALID_BITS;
    }
  }
  img->data = pixels;
  img->width = (int32_t)header->biWidth;
  img->height = (int32_t)header->biHeight;
  return READ_OK;
}

enum close_status fclose_bmp(FILE **in) {
  if (*in == NULL) {
    return CLOSE_ERROR;
  }
  fclose(*in);
  return CLOSE_OK;
}

enum write_status writePixel(FILE *file, struct image *img, int32_t x,
                             int32_t y) {
  struct pixel pixel = img->data[y * img->width + x];
  if (fwrite(&pixel, sizeof(struct pixel), 1, file) != 1) {
    return WRITE_ERROR;
  }
  return WRITE_OK;
}

void bmp_header_init(struct bmp_header *bmpHeader, struct image *img) {
  unsigned int padding = get_padding(img->width);
  bmpHeader->bfType = bType;
  bmpHeader->bfileSize =
      sizeof(struct bmp_header) + (3 * img->width + padding) * img->height;
  bmpHeader->bOffBits = sizeof(struct bmp_header);
  bmpHeader->biSize = bSize;
  bmpHeader->biWidth = img->width;
  bmpHeader->biHeight = img->height;
  bmpHeader->biPlanes = bPlanes;
  bmpHeader->biBitCount = bBitCount;
  bmpHeader->biCompression = bCompression;
  bmpHeader->biSizeImage = (3 * img->width + padding) * img->height;
  bmpHeader->biXPelsPerMeter = bXPelsPerMeter;
  bmpHeader->biYPelsPerMeter = bYPelsPerMeter;
  bmpHeader->biClrUsed = bClrUsed;
  bmpHeader->biClrImportant = bClrImportant;
  bmpHeader->bfReserved = bReserved;
}

enum write_status to_bmp(FILE *file, struct image img) {
  if (file == NULL) {
    return WRITE_ERROR;
  }

  struct bmp_header header = {0};
  struct bmp_header *bmpHeader = &header;
  bmp_header_init(bmpHeader, &img);

  if (fwrite(bmpHeader, sizeof(struct bmp_header), 1, file) != 1) {
    return WRITE_ERROR;
  }

  const int32_t imgWidth = img.width;
  const int32_t imgHeight = img.height;
  unsigned int padding = get_padding(imgWidth);

  for (int32_t y = 0; y < imgHeight; ++y) {
    for (int32_t x = 0; x < imgWidth; ++x) {
      if (writePixel(file, &img, x, y) != WRITE_OK) {
        return WRITE_ERROR;
      }
    }
    for (unsigned int i = 0; i < padding; ++i) {
      if (fputc(0, file) == EOF) {
        return WRITE_ERROR;
      }
    }
  }

  return WRITE_OK;
}
