#include "canvas.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

static const int sin_values[] = {0, 1, 0, -1};
static const int cos_values[] = {1, 0, -1, 0};

int int_sin(int x) {
  x = (x % 360 + 360) % 360;
  int index = x / 90;
  return sin_values[index];
}

int int_cos(int x) {
  x = (x % 360 + 360) % 360;
  int index = x / 90;
  return cos_values[index];
}

struct canvas canvas_init(struct canvas *canvas, int32_t width) {
  width += 1 + width % 2;
  canvas->width = width;
  if (width == 0)
    width = 1;
  canvas->pixels = (struct pixel *)malloc(width * width * sizeof(struct pixel));
  for (int32_t i = 0; i < width * width; i++) {
    canvas->pixels[i].r = 0;
    canvas->pixels[i].g = 0;
    canvas->pixels[i].b = 0;
  }
  canvas->angel = 0;
  canvas->most_left_x = width / 2;
  canvas->most_right_x = -width / 2;
  canvas->most_up_y = width / 2;
  canvas->most_down_y = -width / 2;
  return *canvas;
}

void canvas_destroy(struct canvas *canvas) { free(canvas->pixels); }

struct pixel *canvas_get_pixel(struct canvas *canvas, int32_t x, int32_t y) {
  if (x < 0 || x >= canvas->width || y < 0 || y >= canvas->width)
    return NULL;
  return &canvas->pixels[x + y * canvas->width];
}

void canvas_set_pixel(struct canvas *canvas, int32_t x, int32_t y,
                      struct pixel pixel) {
  if (x < 0 || x >= canvas->width || y < 0 || y >= canvas->width)
    return;
  canvas->pixels[x + y * canvas->width] = pixel;
}

struct pixel *canvas_get_point(int32_t x, int32_t y, struct canvas *canvas) {
  int32_t x0 = canvas->width / 2;
  int32_t y0 = canvas->width / 2;
  if (canvas->width % 2 == 0) {
    x0++;
    y0++;
  }

  return canvas_get_pixel(canvas, x + x0, y + y0);
}

void canvas_set_point(int32_t x, int32_t y, struct pixel pixel,
                      struct canvas *canvas) {
  int32_t x0 = canvas->width / 2;
  int32_t y0 = canvas->width / 2;
  if (canvas->width % 2 == 0) {
    x0++;
    y0++;
  }
  if (x < canvas->most_left_x)
    canvas->most_left_x = x;
  if (x > canvas->most_right_x)
    canvas->most_right_x = x;
  if (y < canvas->most_up_y)
    canvas->most_up_y = y;
  if (y > canvas->most_down_y)
    canvas->most_down_y = y;

  canvas_set_pixel(canvas, x + x0, y + y0, pixel);
}

void canvas_fill(struct canvas *canvas, struct image img, int angel) {
  int32_t y2, x2;
  for (int32_t i = 0; i < img.height; i++) {
    for (int32_t j = 0; j < img.width; j++) {
      x2 = j - img.width / 2;
      y2 = i - img.height / 2;
      if (angel != 0) {
        rotate_coordinates(&x2, &y2, angel);
      }
      canvas_set_point(x2, y2, img.data[i * img.width + j], canvas);
    }
  }
  canvas->angel = angel;
}

struct image canvas_to_image(struct canvas canvas, int32_t x, int32_t y,
                             int32_t width, int32_t height) {

  struct image img = {
      .width = width,
      .height = height,
      .data = (struct pixel *)malloc(sizeof(struct pixel) * width * height)};

  for (int32_t i = 0; i < height; i++) {
    for (int32_t j = 0; j < width; j++) {
      img.data[i * img.width + j] = *canvas_get_point(j + x, i + y, &canvas);
    }
  }
  return img;
}

void rotate_coordinates(int32_t *x, int32_t *y, int angel) {
  angel = -angel;
  int32_t x2 = int_cos(angel) * (*x) - int_sin(angel) * (*y);
  int32_t y2 = int_sin(angel) * (*x) + int_cos(angel) * (*y);
  *x = x2;
  *y = y2;
}
