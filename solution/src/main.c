#include "bmp/bmp_header.h"
#include "bmp/bmp_operations.h"
#include "canvas.h"
#include "common/utils.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))

int main(int argc, char **argv) {

  if (!validate_arguments(argc, argv)) {
    return 1;
  }

  FILE *in;

  if (fopen_bmp_in(&in, argv[1]) != OPEN_OK) {
    fclose_bmp(&in);
    exit_with_error("Error opening file");
    return 1;
  }

  struct image img = {0, 0, NULL};
  int angel = atoi(argv[3]);

  if (from_bmp(in, &img) != READ_OK) {
    fclose_bmp(&in);
    exit_with_error("Error reading file");
    return 1;
  }

  struct canvas canvas;

  canvas_init(&canvas, max(img.width, img.height));

  printf("Angel: %d\n", angel);
  printf("image width: %d, image height: %d\n", img.width, img.height);
  canvas_fill(&canvas, img, angel);

  free(img.data);
  img = canvas_to_image(canvas, canvas.most_left_x, canvas.most_up_y,
                        -canvas.most_left_x + canvas.most_right_x + 1,
                        -canvas.most_up_y + canvas.most_down_y + 1);
  canvas_destroy(&canvas);

  FILE *out;

  if (fopen_bmp_out(&out, argv[2]) != OPEN_OK) {
    free(img.data);
    fclose_bmp(&out);
    exit_with_error("Error opening file");
    return 1;
  }

  if (to_bmp(out, img) != WRITE_OK) {
    free(img.data);
    fclose_bmp(&out);
    exit_with_error("Error writing to file");
    return 1;
  }

  free(img.data);

  if (fclose_bmp(&out) != CLOSE_OK) {
    exit_with_error("Error closing file");
    return 1;
  }

  if (fclose_bmp(&in) != CLOSE_OK) {
    exit_with_error("Error closing file");
    return 1;
  }

  return 0;
}
