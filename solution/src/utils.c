#include "common/utils.h"
#include <stdio.h>
#include <stdlib.h>

int validate_arguments(int argc, char **argv) {
  if (argc != 4) {
    printf("Usage: %s <input file> <output file> <angel>\n", argv[0]);
    return 0;
  }

  return 1;
}

void exit_with_error(const char *msg) {
  perror(msg);
  exit(EXIT_FAILURE);
}
