#pragma once

#include "common/pixel.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>

struct canvas {
  int32_t width;
  struct pixel *pixels;
  int angel;
  int32_t most_left_x;
  int32_t most_right_x;
  int32_t most_up_y;
  int32_t most_down_y;
};

struct canvas canvas_init(struct canvas *canvas, int32_t width);
void canvas_destroy(struct canvas *canvas);
struct pixel *canvas_get_pixel(struct canvas *canvas, int32_t x, int32_t y);
void canvas_set_pixel(struct canvas *canvas, int32_t x, int32_t y,
                      struct pixel pixel);
struct pixel *canvas_get_point(int32_t x, int32_t y, struct canvas *canvas);
void canvas_set_point(int32_t x, int32_t y, struct pixel pixel,
                      struct canvas *canvas);
void canvas_fill(struct canvas *canvas, struct image img, int angel);
struct image canvas_to_image(struct canvas canvas, int32_t x, int32_t y,
                             int32_t width, int32_t height);
void rotate_coordinates(int32_t *x, int32_t *y, int angel);
