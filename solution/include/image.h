#pragma once
#include "common/pixel.h"
#include <stdint.h>
#include <string.h>

struct image {
  int32_t width, height;
  struct pixel *data;
};
