#pragma once

#include "bmp_header.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum read_status {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR
};

enum read_status from_bmp(FILE *file, struct image *img);

enum write_status { WRITE_OK = 0, WRITE_ERROR };

enum write_status to_bmp(FILE *file, struct image img);

enum open_status { OPEN_OK = 0, OPEN_ERROR };

enum open_status fopen_bmp_in(FILE **in, char const *file_name);

enum open_status fopen_bmp_out(FILE **in, char const *file_name);

enum close_status { CLOSE_OK = 0, CLOSE_ERROR };

enum close_status fclose_bmp(FILE **in);
